#!/usr/bin/awk
BEGIN{
# separates fields by comma
FS=","
# initialized sum variable
sum=0
}
{
# added each sales figure to the sum
sum+=$3
}
END{
# prints sum
print "SUM: " sum
}
