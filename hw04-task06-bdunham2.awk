#!/usr/bin/awk
# separates fields by comma
BEGIN{FS=","}
{
# conditional statement that evaluates if the number of fields is <= 4
if (NF<=4)
# if true, prints the line number 
print NR
}
