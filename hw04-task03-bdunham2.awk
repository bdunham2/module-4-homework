#!/usr/bin/awk
# separates fields by comma
BEGIN{FS=","}
# conditional statement that evaluates grades higher than 75
{
if ($2 > 75){
# if true, prints student's name
print $1}
}
