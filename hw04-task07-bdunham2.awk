#!/usr/bin/awk
BEGIN{
# separates fields by comma
FS=","
# initialized maxVotes variables to hold highest number of votes
maxVotes=0
# initialized breed variable to first breed option
breed=$1}
{
# if statement that evaluates which breed has the most votes
if (maxVotes<$2){
# if true, then that breed and max votes are saved in variables
maxVotes=$2
breed=$1}
}
# prints winning breed and number of votes it got
END{print (breed " has" maxVotes " votes")}
