#!/usr/bin/awk
# separates field at comma
BEGIN{FS=","}
# conditional statements evaluating if student is online or onsite
{
# prints names of online students
if ($3 == "online") {printf ("%s is an online student\n", $1)}
# prints names of onsite students
else if ($3 == "onsite") {printf ("%s is an onsite student\n", $1)}
}
