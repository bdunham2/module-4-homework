#!/usr/bin/awk
# separates fields by comma
BEGIN{FS=","}
{
# conditional statement for if students are online
if ($3 == "online") {
# if true, prints student's name
print $1}
}
