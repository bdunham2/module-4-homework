#!/usr/bin/awk
# separates fields by comma
BEGIN{FS=","}
{
# initialized sumTotal variable at 0
sumTotal=0
# for loop that loops through every value in the file
for (i=1;i<100;i++){
# added values to sumTotal
sumTotal=sumTotal+$i
}
# prints sumTotal
print sumTotal
}
