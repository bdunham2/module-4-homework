#!/usr/bin/awk
BEGIN{
# separates fields 
FS="##"
# initialized array
array[0]=""
# initialized variable to hold length of the array
arrayLength=0
}
{
# conditional statement that checks if movie title has a number
if (!match($1,/.*[0-9]+.*$/)) {
# if movie title does not have a number, then it is added to the array
array[arrayLength++]=$1;}s
}
END{
# for loop that prints every item in the array
for (i=0; i < arrayLength; i++)
print array[i]
}
