#!/usr/bin/awk
BEGIN{
# separates fields by comma
FS=","
# initialized sum and counter variables
sum=0
counter=-1
}
{
# added each sales figure to the sum
sum+=$3
# adds one value to counter for each column
counter+=1
}
END{
# calculates and prints average
print "AVG: " sum/counter
}
