#!/usr/bin/awk
# separates every field by comma
BEGIN{FS=","}
# prints name and grade fields
{print $1, $2}
